(function() {
  'use strict';

  angular
    .module('generator')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/generator/:username',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('register', {
        url: '/',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      .state('share', {
        url: '/share',
        templateUrl: 'app/share/share.html',
        controller: 'ShareController',
        controllerAs: 'share'
      })
      .state('verification', {
        url: '/verification',
        templateUrl: 'app/cpa-grip/cpa-grip.html',
        controller: 'cpaGripController',
        controllerAs: 'cpa'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
