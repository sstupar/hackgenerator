(function() {
  'use strict';

  angular
    .module('generator')
    .directive('progressBar', progressBar);

  /** @ngInject */
  function progressBar($state) {
    var directive = {
      templateUrl: 'app/components/generator/progress-bar/progress-bar.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){
      // scope.checkUserName = function () {
      //   if(!scope.userName){
      //     scope.showError = true;
      //   } else {
      //     scope.showError = false;
      //   }
      // };
          // $(".Download1").ElasticProgress({
          //     buttonSize: 60,
          //     fontFamily: "Montserrat",
          //     colorBg: "#adeca8",
          //     colorFg: "#7cc576",
          //     onClick: function(event) {
          //         console.log("onClick");
          //         $(this).ElasticProgress("open");
          //     },
          //     onOpen: function(event) {
          //         console.log("onOpen");
          //         fakeLoading($(this));
          //     },
          //     onComplete: function(event) {
          //         console.log("onComplete");
          //     },
          //     onClose: function(event) {
          //         console.log("onClose");
          //     },
          //     onFail: function(event) {
          //         console.log("onFail");
          //         $(this).ElasticProgress("open");
          //     },
          //     onCancel: function(event) {
          //         console.log("onCancel");
          //         $(this).ElasticProgress("open");
          //     }
          // });

          // $(".Download2").ElasticProgress({
          //     align: "center",
          //     fontFamily: "Roboto",
          //     colorFg: "#77c2ff",
          //     colorBg: "#4e80dd",
          //     bleedTop: 110,
          //     bleedBottom: 40,
          //     buttonSize: 100,
          //     labelTilt: 70,
          //     arrowDirection: "up",
          //     onClick: function() {
          //         $(this).ElasticProgress("open");
          //         //$(this).ElasticProgress("close");
          //     },
          //     onOpen: function() {
          //         fakeLoading($(this))
          //     },
          //     onCancel: function() {
          //         $(this).ElasticProgress("close");
          //     },
          //     onComplete: function() {
          //         var $obj = $(this)
          //
          //         $obj.ElasticProgress("close");
          //     }
          // });
          scope.openProgress = function() {
            if(!scope.userName){
              scope.showError = true;
              scope.$digest();
              return;
            }
            scope.showError = false;
            scope.$digest();
            $('.Download3').ElasticProgress("open");
          };
          $(".Download3").ElasticProgress({
              align: "center",
              colorFg: "#686e85",
              colorBg: "#b4bad2",
              highlightColor: "#ffab91",
              // width: Math.min($(window).width()/2 - 100, 600),
              // width: 200,
              barHeight: 10,
              labelHeight: 50,
              labelWobbliness: 0,
              bleedTop: 120,
              bleedRight: 100,
              buttonSize: 60,
              fontFamily: "Arvo",
              barStretch: 0,
              barInset: 4,
              barElasticOvershoot: 1,
              barElasticPeriod: 0.6,
              textFail: "Download Failed",
              textComplete: "OK",
              arrowHangOnFail: false,
              arrowDirection: 'up',
              // onClick: function() {
              //   if(!scope.userName){
              //     scope.showError = true;
              //     scope.$digest();
              //     return;
              //   }
              //   scope.showError = false;
              //   scope.$digest();
              //   $(this).ElasticProgress("open");
              // },
              onClick: scope.openProgress,
              onOpen: function() {
                  fakeLoading($(this));
                  var userMessage = '<h3 class="connecting">Connecting user ' + scope.userName + ' to database ...</h3>';
                  $(userMessage).insertAfter('.Download3');
              },
              onComplete: function() {
                  var $obj = $(this)
                  $state.transitionTo('home', {username: scope.userName});
              }
          });

          // var e = new ElasticProgress(document.querySelectorAll('.Download')[3], {
          //     colorFg: "#ed7499",
          //     colorBg: "#635c73",
          //     highlightColor: "#ed7499",
          //     barHeight: 14,
          //     barInset: 10,
          //     fontFamily: "Indie Flower"
          // });
          // e.onClick(function() {
          //     e.open();
          // })
          // e.onOpen(function() {
          //     fakeLoading(e, 2, 0.5);
          // });
          // e.onFail(function() {
          //     e.close();
          // })

          function fakeLoading($obj, speed, failAt) {
              if (typeof speed == "undefined") speed = 2;
              if (typeof failAt == "undefined") failAt = -1;
              var v = 0;
              var l = function() {
                  if (failAt > -1) {
                      if (v >= failAt) {
                          if (typeof $obj.jquery != "undefined") {
                              $obj.ElasticProgress("fail");
                          } else {
                              $obj.fail();
                          }
                          return;
                      }
                  }
                  v += Math.pow(Math.random(), 2) * 0.1 * speed;

                  if (typeof $obj.jquery != "undefined") {
                      $obj.ElasticProgress("setValue", v);
                  } else {
                      $obj.setValue(v);
                  }
                  if (v < 1) {
                      TweenMax.delayedCall(0.05 + (Math.random() * 0.14), l)
                  }
              };
              l();
          }
    }
  }

})();
