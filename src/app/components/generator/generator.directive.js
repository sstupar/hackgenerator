(function() {
  'use strict';

  angular
    .module('generator')
    .directive('generator', generator);

  /** @ngInject */
  function generator($compile, $state) {
    var directive = {
      templateUrl: 'app/components/generator/generator.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){

      scope.resources = [
        'Gold',
        'Elixir',
        'Gems'
      ]
      scope.resourceValues = [
        100,
        500,
        1000,
        10000,
        100000,
        'MAX VALUE'
      ]

      scope.generateResources = function(){
        var cardCircle = element.find('.card_circle');
        cardCircle.css({
          '-webkit-transition': 'height 1s',
          'height': '150%',
          'position': 'absolute',
          'opacity': 1,
          'z-index': '50',
          'border-radius': '0',
          'margin-top': '-270px'
        });
        var gear = '<div gear></div>';
        var gearDir = $compile(gear)( scope );
        cardCircle.append(gearDir);
      }

    }
  }

})();
