(function() {
  'use strict';

  angular
    .module('generator')
    .directive('userMenu', userMenu);

  /** @ngInject */
  function userMenu($state, $mdDialog) {
    var directive = {
      templateUrl: 'app/components/generator/user-menu/user-menu.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){

       scope.showAlert = function(ev) {
         // Appending dialog to document.body to cover sidenav in docs app
         // Modal dialogs should fully cover application
         // to prevent interaction outside of dialog
         $mdDialog.show(
           $mdDialog.alert()
             .parent(angular.element(document.querySelector('#popupContainer')))
             .clickOutsideToClose(true)
             .title('Clash of Clans resource generator')
             .content('Copyright © 2015 Clash of Clans hack.')
             .ariaLabel('Alert Dialog Demo')
             .ok('Close')
             .targetEvent(ev)
         );
       };

      scope.transitionTo = function(state){
          $state.transitionTo(state);
      };

      $('.btn-menu').on('click', function(event) {
        element.find('li.drop').toggleClass('active');
  event.preventDefault();
  if ($(this).hasClass('menu-open')) {
      $(this).removeClass('menu-open').addClass('menu-closed');
  } else {
      $(this).removeClass('menu-closed').addClass('menu-open');
  };
});

  $('.dropOut ul').on('click', 'li', function(event) {
    var btnMenu = $('.btn-menu');
    if (btnMenu.hasClass('menu-open')) {
        btnMenu.removeClass('menu-open').addClass('menu-closed');
    }
    element.find('li.drop').removeClass('active')
  });
    }
  }

})();
