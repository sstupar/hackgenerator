(function() {
  'use strict';

  angular
    .module('generator')
    .directive('gear', gear);

  /** @ngInject */
  function gear($state) {
    var directive = {
      templateUrl: 'app/components/generator/gear/gear.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){

        scope.share = function(){

          $state.transitionTo('share');
        }

        if(scope.isActiveG && !scope.someModelG){
          scope.someModelG = 'MAX VALUE';
        }
        if(scope.isActiveW &&!scope.someModelW){
          scope.someModelW = 'MAX VALUE';
        }
        if(scope.isActiveI && !scope.someModelI){
          scope.someModelI = 'MAX VALUE';
        }
        var generatingG = scope.someModelG ? 'Generating ' + scope.someModelG + ' ' + scope.resources[0] : '';
        var generatingW = scope.someModelW ? 'Generating ' + scope.someModelW + ' ' + scope.resources[1] : '';
        var generatingI = scope.someModelI ? 'Generating ' + scope.someModelI + ' ' + scope.resources[2] : '';
        scope.initTextList = [
          'Connecting to server',
          'Registering user',
          'Generating resources',
          generatingG,
          generatingW,
          generatingI,
          'Human verification requried'
        ];
        scope.initTextList = scope.initTextList.filter(Boolean);

        var count = -1;
        var interval = setInterval(function(){
          count++;
          scope.initText = scope.initTextList[count];
          scope.$digest();
          if(count === scope.initTextList.length){
            scope.initText = scope.initTextList.pop();
            var request = '<span>Please follow this steps to claim your resources.</span>'
            var initText = element.find('.init-text');
            initText.css({
              color: 'red'
            });
            initText.append(request);


            if(scope.someModelG){
                element.find('.generated-info').append('<div><span>Generated '+ scope.resources[0] +':</span> '+ scope.someModelG + '</div>');
            }
            if(scope.someModelW){
                element.find('.generated-info').append('<div><span>Generated '+ scope.resources[1] +':</span> '+ scope.someModelW + '</div>');
            }
            if(scope.someModelI){
                element.find('.generated-info').append('<div><span>Generated '+ scope.resources[2] +':</span> '+ scope.someModelI + '</div>');
            }
            scope.$digest();
            element.find('.green').css({
              'animation': 'none'
            });
            element.find('.yellow').css({
              'animation': 'none'
            });
            scope.showClaimButton = true;
            scope.$digest();

            clearInterval(interval);
          }

        }, 1000);

    }
  }

})();
