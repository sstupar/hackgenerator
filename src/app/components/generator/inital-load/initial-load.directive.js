(function() {
  'use strict';

  angular
    .module('generator')
    .directive('initialLoad', initialLoad);

  /** @ngInject */
  function initialLoad($timeout) {
    var directive = {
      templateUrl: 'app/components/generator/inital-load/initial-load.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){
      setTimeout(function(){
        element.remove();
      }, 3500)
    }

  }

})();
