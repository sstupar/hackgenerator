(function() {
  'use strict';

  angular
    .module('generator')
    .directive('loginForm', loginForm);

  /** @ngInject */
  function loginForm() {
    var directive = {
      templateUrl: 'app/components/generator/login-form/login-form.template.html',
      link: linkFunc
    };

    return directive;

    function linkFunc(scope, element, attr){
      scope.showError = false;
      // scope.checkUserName = function () {
      //   if(!scope.userName){
      //     scope.showError = true;
      //   } else {
      //     scope.showError = false;
      //   }
      // };
      scope.submit = function(){
        if(!scope.userName){
            element.find('.submit').click();
            element.find('.input input').focus();
        }

      };
      element.find('.username').on('keyup', function(e){
        if(e.keyCode !== 13){
          return;
        }

        $(this).blur();
        scope.openProgress();
      });

      $( ".input" ).focusin(function() {
      $( this ).find( "span" ).animate({"opacity":"0"}, 200);
      });

      $( ".input" ).focusout(function() {
      $( this ).find( "span" ).animate({"opacity":"1"}, 300);
      });

      $(".login").submit(function(){
      $(this).find(".submit i").removeAttr('class').addClass("fa fa-check").css({"color":"#fff"});
      $(".submit").css({"background":"#2ecc71", "border-color":"#2ecc71"});
      $(".feedback").show().animate({"opacity":"1", "bottom":"-80px"}, 400);
      $("input").css({"border-color":"#2ecc71"});
      return false;
});
    }
  }

})();
