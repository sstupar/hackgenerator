(function() {
  'use strict';

  angular
    .module('generator')
    .controller('ShareController', ShareController);

  /** @ngInject */
  function ShareController($timeout,$state, $scope) {
    var vm = this;
    vm.nextStep = function(){
      $state.transitionTo('verification');
    };
  }
})();
