(function() {
  'use strict';

  angular
    .module('generator', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'restangular',
      'ui.router',
      'ngMaterial',
      '720kb.socialshare'
    ]);

})();
